package ru.t1.gorodtsova.tm.dto.response.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.model.Task;

@NoArgsConstructor
public final class TaskShowByIdResponse extends AbstractTaskResponse {

    public TaskShowByIdResponse(@Nullable final Task task) {
        super(task);
    }

}
