package ru.t1.gorodtsova.tm.dto.request.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractIndexRequest;

@NoArgsConstructor
public final class ProjectRemoveByIndexRequest extends AbstractIndexRequest {

    public ProjectRemoveByIndexRequest(@Nullable final String token, @Nullable final Integer index) {
        super(token, index);
    }

}
