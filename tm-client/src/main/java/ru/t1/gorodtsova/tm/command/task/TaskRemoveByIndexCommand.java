package ru.t1.gorodtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.request.task.TaskRemoveByIndexRequest;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Remove task by index";

    @NotNull
    private final String NAME = "task-remove-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY INDEX]");
        System.out.println("ENTER INDEX");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(getToken(), index);
        getTaskEndpoint().removeTaskByIndex(request);
    }

}
