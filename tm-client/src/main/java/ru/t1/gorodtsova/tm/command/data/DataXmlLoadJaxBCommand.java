package ru.t1.gorodtsova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.request.domain.DataXmlLoadJaxBRequest;

public final class DataXmlLoadJaxBCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load data from xml file";

    @NotNull
    private final String NAME = "data-load-xml-jaxb";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DATA LOAD XML]");
        @NotNull final DataXmlLoadJaxBRequest request = new DataXmlLoadJaxBRequest(getToken());
        getDomainEndpoint().loadDataXmlJaxb(request);
    }

}
