package ru.t1.gorodtsova.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    private final String DESCRIPTION = "Create new project";

    @NotNull
    private final String NAME = "project-create";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectCreateRequest request = new ProjectCreateRequest(getToken(), name, description);
        getProjectEndpoint().createProject(request);
    }

}
