package ru.t1.gorodtsova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.request.task.TaskStartByIndexRequest;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class TaskStartByIndexCommand extends AbstractTaskCommand {

    @NotNull
    private final String DESCRIPTION = "Start task by index";

    @NotNull
    private final String NAME = "task-start-by-index";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(getToken(), index);
        getTaskEndpoint().startTaskByIndex(request);
    }

}
