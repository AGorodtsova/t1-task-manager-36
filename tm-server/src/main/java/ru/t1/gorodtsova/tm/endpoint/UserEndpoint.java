package ru.t1.gorodtsova.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.endpoint.IUserEndpoint;
import ru.t1.gorodtsova.tm.api.service.IServiceLocator;
import ru.t1.gorodtsova.tm.api.service.IUserService;
import ru.t1.gorodtsova.tm.dto.request.user.*;
import ru.t1.gorodtsova.tm.dto.response.user.*;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.model.Session;
import ru.t1.gorodtsova.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@NoArgsConstructor
@WebService(endpointInterface = "ru.t1.gorodtsova.tm.api.endpoint.IUserEndpoint")
public class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull IServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        return getServiceLocator().getUserService();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLockResponse lockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserLockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().lockUserByLogin(login);
        return new UserLockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserUnlockResponse unlockUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUnlockRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        getUserService().unlockUserByLogin(login);
        return new UserUnlockResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegistryResponse registryUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRegistryRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable final String password = request.getPassword();
        @Nullable final String email = request.getEmail();
        @Nullable User user = getUserService().create(login, password, email);
        return new UserRegistryResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveResponse removeUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserRemoveRequest request
    ) {
        check(request, Role.ADMIN);
        @Nullable final String login = request.getLogin();
        @Nullable User user = getUserService().removeByLogin(login);
        return new UserRemoveResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateProfileResponse updateProfileUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserUpdateProfileRequest request
    ) {
        check(request);
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        @Nullable final String firstName = request.getFirstName();
        @Nullable final String lastName = request.getLastName();
        @Nullable final String middleName = request.getMiddleName();
        @Nullable User user = getUserService().updateUser(userId, firstName, lastName, middleName);
        return new UserUpdateProfileResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse viewProfileUser(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserProfileRequest request
    ) {
        check(request);
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        if (userId == null) return new UserProfileResponse();
        @Nullable User user = getUserService().findOneById(userId);
        return new UserProfileResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserChangePasswordResponse changePassword(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull final UserChangePasswordRequest request
    ) {
        check(request);
        @NotNull final Session session = check(request);
        @Nullable final String userId = session.getUserId();
        if (userId == null) return new UserChangePasswordResponse();
        @Nullable User user = getUserService().setPassword(userId, request.getPassword());
        return new UserChangePasswordResponse(user);
    }

}
