package ru.t1.gorodtsova.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    @NotNull
    M add(@Nullable M model);

    @NotNull
    Collection<M> add(@NotNull Collection<M> models);

    @NotNull
    Collection<M> set(@NotNull Collection<M> models);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable Comparator<M> comparator);

    @Nullable
    M findOneById(@Nullable String Id);

    @NotNull
    M findOneByIndex(@Nullable Integer index);

    @Nullable
    M removeOne(@Nullable M model);

    @Nullable
    M removeOneById(@Nullable String Id);

    @Nullable
    M removeOneByIndex(@Nullable Integer index);

    void removeAll();

    void removeAll(@Nullable Collection<M> collection);

    boolean existsById(@Nullable String id);

    int getSize();

}
