package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.gorodtsova.tm.api.repository.IProjectRepository;
import ru.t1.gorodtsova.tm.api.repository.ITaskRepository;
import ru.t1.gorodtsova.tm.api.service.IProjectTaskService;
import ru.t1.gorodtsova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.gorodtsova.tm.exception.entity.TaskNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.TaskIdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.UserIdEmptyException;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.repository.ProjectRepository;
import ru.t1.gorodtsova.tm.repository.TaskRepository;

import static ru.t1.gorodtsova.tm.constant.ProjectTestData.*;
import static ru.t1.gorodtsova.tm.constant.TaskTestData.*;
import static ru.t1.gorodtsova.tm.constant.UserTestData.USER1;

@Category(UnitCategory.class)
public class ProjectTaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void before() {
        projectRepository.add(USER1_PROJECT_LIST);
        taskRepository.add(USER1_TASK_LIST);
    }

    @After
    public void after() {
        projectRepository.removeAll(USER1_PROJECT_LIST);
        taskRepository.removeAll(USER1_TASK_LIST);
    }

    @Test
    public void bindTaskToProject() {
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertEquals(USER1_PROJECT1.getId(), USER1_TASK1.getProjectId());
        Assert.assertNotEquals(USER1_PROJECT2.getId(), USER1_TASK1.getProjectId());

        thrown.expect(UserIdEmptyException.class);
        projectTaskService.bindTaskToProject(null, USER1_PROJECT1.getId(), USER1_TASK1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        projectTaskService.bindTaskToProject(USER1.getId(), null, USER1_TASK1.getId());

        thrown.expect(TaskIdEmptyException.class);
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), null);

        thrown.expect(ProjectNotFoundException.class);
        projectTaskService.bindTaskToProject(USER1.getId(), USER2_PROJECT1.getId(), USER1_TASK1.getId());

        thrown.expect(TaskNotFoundException.class);
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER2_TASK1.getId());
    }

    @Test
    public void unbindTaskFromProject() {
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNotNull(USER1_TASK1.getProjectId());
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        Assert.assertNull(USER1_TASK1.getProjectId());

        thrown.expect(UserIdEmptyException.class);
        projectTaskService.unbindTaskFromProject(null, USER1_PROJECT1.getId(), USER1_TASK1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        projectTaskService.unbindTaskFromProject(USER1.getId(), null, USER1_TASK1.getId());

        thrown.expect(TaskIdEmptyException.class);
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), null);

        thrown.expect(ProjectNotFoundException.class);
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER2_PROJECT1.getId(), USER1_TASK1.getId());

        thrown.expect(TaskNotFoundException.class);
        projectTaskService.unbindTaskFromProject(USER1.getId(), USER1_PROJECT1.getId(), USER2_TASK1.getId());
    }

    @Test
    public void removeProjectById() {
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK1.getId());
        projectTaskService.bindTaskToProject(USER1.getId(), USER1_PROJECT1.getId(), USER1_TASK2.getId());
        projectTaskService.removeProjectById(USER1.getId(), USER1_PROJECT1.getId());
        Assert.assertNull(projectRepository.findOneById(USER1_PROJECT1.getId()));
        Assert.assertNull(taskRepository.findOneById(USER1_TASK1.getId()));
        Assert.assertNull(taskRepository.findOneById(USER1_TASK2.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectTaskService.removeProjectById(null, USER1_PROJECT1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        projectTaskService.removeProjectById(USER1.getId(), null);

        thrown.expect(ProjectNotFoundException.class);
        projectTaskService.removeProjectById(USER1.getId(), USER2_PROJECT1.getId());
    }

}
