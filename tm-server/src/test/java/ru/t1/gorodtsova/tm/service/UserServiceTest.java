package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.gorodtsova.tm.api.repository.IProjectRepository;
import ru.t1.gorodtsova.tm.api.repository.ITaskRepository;
import ru.t1.gorodtsova.tm.api.repository.IUserRepository;
import ru.t1.gorodtsova.tm.api.service.IPropertyService;
import ru.t1.gorodtsova.tm.api.service.IUserService;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.exception.entity.UserNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.IdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.IndexIncorrectException;
import ru.t1.gorodtsova.tm.exception.field.LoginEmptyException;
import ru.t1.gorodtsova.tm.exception.field.PasswordEmptyException;
import ru.t1.gorodtsova.tm.exception.user.ExistsEmailException;
import ru.t1.gorodtsova.tm.exception.user.ExistsLoginException;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.model.User;
import ru.t1.gorodtsova.tm.repository.ProjectRepository;
import ru.t1.gorodtsova.tm.repository.TaskRepository;
import ru.t1.gorodtsova.tm.repository.UserRepository;
import ru.t1.gorodtsova.tm.util.HashUtil;

import static ru.t1.gorodtsova.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IUserService userService = new UserService(propertyService, userRepository, taskRepository, projectRepository);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup() {
        userRepository.add(USER_LIST);
    }

    @After
    public void tearDown() {
        userRepository.removeAll();
    }

    @Test
    public void add() {
        userService.removeAll();
        userService.add(USER1);
        Assert.assertEquals(USER1, userService.findAll().get(0));
    }

    @Test
    public void addAll() {
        userService.removeAll();
        userService.add(USER_LIST1);
        Assert.assertEquals(USER_LIST1, userService.findAll());

    }

    @Test
    public void set() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.set(USER_LIST1);
        Assert.assertEquals(USER_LIST1, userService.findAll());
        Assert.assertNotEquals(USER_LIST2, userService.findAll());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(USER_LIST, userService.findAll());
    }

    @Test
    public void findOneById() {
        Assert.assertEquals(USER1, userService.findOneById(USER1.getId()));
        Assert.assertNotEquals(USER1, userService.findOneById(USER2.getId()));
        @NotNull final User user = new User();
        Assert.assertNull(userService.findOneById(user.getId()));

        thrown.expect(IdEmptyException.class);
        userService.findOneById(null);
    }

    @Test
    public void findOneByIndex() {
        final int index = USER_LIST1.indexOf(USER1);
        Assert.assertEquals(USER1, userService.findOneByIndex(index));
        Assert.assertNotEquals(USER2, userService.findOneByIndex(index));

        thrown.expect(IndexIncorrectException.class);
        userService.findOneByIndex(null);
        userService.findOneByIndex(USER_LIST.size() + 1);
    }

    @Test
    public void removeAll() {
        Assert.assertFalse(userService.findAll().isEmpty());
        userService.removeAll();
        Assert.assertTrue(userService.findAll().isEmpty());
    }

    @Test
    public void removeAllCollection() {
        Assert.assertEquals(USER_LIST, userService.findAll());
        userService.removeAll(USER_LIST1);
        Assert.assertFalse(userService.findAll().contains(USER2));
        Assert.assertTrue(userService.findAll().contains(ADMIN1));
    }

    @Test
    public void removeOneById() {
        Assert.assertEquals(USER1, userService.removeOneById(USER1.getId()));
        Assert.assertFalse(userService.findAll().contains(USER1));

        thrown.expect(IdEmptyException.class);
        userService.removeOneById(null);
    }

    @Test
    public void removeOneByIndex() {
        final int index = USER_LIST.indexOf(USER1);
        Assert.assertEquals(USER1, userService.removeOneByIndex(index));
        Assert.assertFalse(userService.findAll().contains(USER1));

        thrown.expect(IndexIncorrectException.class);
        userService.removeOneByIndex(null);

        thrown.expect(IndexOutOfBoundsException.class);
        userService.removeOneByIndex(USER_LIST.size() + 1);
    }

    @Test
    public void existsById() {
        Assert.assertTrue(userService.existsById(USER1.getId()));
        @NotNull final User user = new User();
        Assert.assertFalse(userService.existsById(user.getId()));
        Assert.assertFalse(userService.existsById(null));
    }

    @Test
    public void createUserLoginPass() {
        @NotNull final User user = userService.create("testuser", "testpass");
        Assert.assertEquals(user, userService.findOneById(user.getId()));
        Assert.assertEquals("testuser", user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, "testpass"), user.getPasswordHash());

        thrown.expect(LoginEmptyException.class);
        userService.create(null, USER1.getPasswordHash());

        thrown.expect(ExistsLoginException.class);
        userService.create(USER1.getLogin(), USER1.getPasswordHash());

        thrown.expect(PasswordEmptyException.class);
        userService.create("login", null);
    }

    @Test
    public void createUserLoginPassEmail() {
        @NotNull User user = userService.create("testuser", "testpass", "user@company.ru");
        Assert.assertTrue(userService.existsById(user.getId()));
        Assert.assertEquals("testuser", user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, "testpass"), user.getPasswordHash());
        Assert.assertEquals("user@company.ru", user.getEmail());
    }

    @Test
    public void createUserLoginPassRole() {
        @NotNull User user = userService.create("testuser", "testpass", Role.USUAL);
        Assert.assertTrue(userService.existsById(user.getId()));
        Assert.assertEquals("testuser", user.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, "testpass"), user.getPasswordHash());
        Assert.assertEquals(Role.USUAL, user.getRole());
    }

    @Test
    public void updateUser() {
        userService.updateUser(USER1.getId(), "new first name", "new last name", "new middle name");
        Assert.assertEquals("new first name", USER1.getFirstName());
        Assert.assertEquals("new last name", USER1.getLastName());
        Assert.assertEquals("new middle name", USER1.getMiddleName());

        thrown.expect(IdEmptyException.class);
        userService.updateUser(null, "new first name", "new last name", "new description");

        @NotNull final User user = new User();
        thrown.expect(UserNotFoundException.class);
        userService.updateUser(user.getId(), "new first name", "new last name", "new middle name");
    }

    @Test
    public void findByLogin() {
        Assert.assertEquals(USER1, userService.findByLogin(USER1.getLogin()));
        Assert.assertNotEquals(USER1, userService.findByLogin(USER2.getLogin()));

        thrown.expect(LoginEmptyException.class);
        userService.findByLogin(null);

        @NotNull final User user = new User();
        user.setLogin("login");
        thrown.expect(UserNotFoundException.class);
        userService.findByLogin(user.getLogin());
    }

    @Test
    public void findByEmail() {
        Assert.assertEquals(USER1, userService.findByEmail(USER1.getEmail()));
        Assert.assertNotEquals(USER1, userService.findByEmail(USER2.getEmail()));

        thrown.expect(ExistsEmailException.class);
        userService.findByEmail(null);

        @NotNull final User user = new User();
        user.setEmail("user@company.ru");
        thrown.expect(UserNotFoundException.class);
        userService.findByEmail(user.getEmail());
    }

    @Test
    public void isLoginExist() {
        Assert.assertTrue(userService.isLoginExist(USER1.getLogin()));
        Assert.assertFalse(userService.isLoginExist("NOT_EXIST_LOGIN"));
        Assert.assertFalse(userService.isLoginExist(""));
    }

    @Test
    public void isEmailExist() {
        Assert.assertTrue(userService.isEmailExist(USER1.getEmail()));
        Assert.assertFalse(userService.isEmailExist("NOT_EXIST_EMAIL"));
        Assert.assertFalse(userService.isEmailExist(""));
    }

    @Test
    public void setPassword() {
        userService.setPassword(USER1.getId(), "new_password");
        Assert.assertEquals(HashUtil.salt(propertyService, "new_password"), USER1.getPasswordHash());

        thrown.expect(IdEmptyException.class);
        userService.setPassword(null, "new_password");

        thrown.expect(PasswordEmptyException.class);
        userService.setPassword(USER1.getId(), "");

        @NotNull final User user = new User();
        user.setLogin("login");
        user.setPasswordHash("password");
        thrown.expect(UserNotFoundException.class);
        userService.setPassword(user.getId(), "new_password");

    }

    @Test
    public void lockUserByLogin() {
        Assert.assertFalse(USER1.getLocked());
        userService.lockUserByLogin(USER1.getLogin());
        Assert.assertTrue(USER1.getLocked());

        @NotNull final User user = new User();
        thrown.expect(LoginEmptyException.class);
        userService.lockUserByLogin(user.getLogin());
    }

    @Test
    public void unlockUserByLogin() {
        USER1.setLocked(true);
        Assert.assertTrue(USER1.getLocked());
        userService.unlockUserByLogin(USER1.getLogin());
        Assert.assertFalse(USER1.getLocked());

        @NotNull final User user = new User();
        thrown.expect(LoginEmptyException.class);
        userService.unlockUserByLogin(user.getLogin());
    }

}
