package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.gorodtsova.tm.api.repository.ITaskRepository;
import ru.t1.gorodtsova.tm.api.service.ITaskService;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.*;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.model.Task;
import ru.t1.gorodtsova.tm.repository.TaskRepository;

import static ru.t1.gorodtsova.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.gorodtsova.tm.constant.ProjectTestData.USER2_PROJECT1;
import static ru.t1.gorodtsova.tm.constant.TaskTestData.*;
import static ru.t1.gorodtsova.tm.constant.UserTestData.USER1;
import static ru.t1.gorodtsova.tm.constant.UserTestData.USER2;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        taskRepository.add(TASK_LIST);
    }

    @After
    public void tearDown() {
        taskRepository.removeAll();
    }

    @Test
    public void add() {
        taskService.removeAll();
        taskService.add(USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, taskService.findAll().get(0));
    }

    @Test
    public void addByUser() {
        taskService.removeAll();
        taskService.add(USER1.getId(), USER1_TASK1);
        Assert.assertEquals(USER1_TASK1, taskService.findAll().get(0));
        Assert.assertEquals(USER1.getId(), taskService.findAll().get(0).getUserId());
        Assert.assertNull(USER1.getId(), null);
        thrown.expect(UserIdEmptyException.class);
        taskService.add(null, USER1_TASK2);
    }

    @Test
    public void set() {
        Assert.assertFalse(taskService.findAll().isEmpty());
        taskService.set(USER1_TASK_LIST);
        Assert.assertEquals(USER1_TASK_LIST, taskService.findAll());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(TASK_LIST, taskService.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(USER1_TASK_LIST, taskService.findAll(USER1.getId()));
        Assert.assertNotEquals(USER1_TASK_LIST, taskService.findAll(USER2.getId()));
        thrown.expect(UserIdEmptyException.class);
        taskService.findAll("");
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertEquals(USER1_TASK1, taskService.findOneById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertNull(taskService.findOneById(USER1.getId(), USER2_TASK1.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.findOneById(null, USER1_TASK1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        taskService.findOneById(USER1.getId(), null);
    }

    @Test
    public void findOneByIndexByUserId() {
        final int index = USER1_TASK_LIST.indexOf(USER1_TASK2);
        Assert.assertEquals(USER1_TASK2, taskService.findOneByIndex(USER1.getId(), index));

        thrown.expect(UserIdEmptyException.class);
        taskService.findOneByIndex(null, index);

        thrown.expect(IndexIncorrectException.class);
        taskService.findOneByIndex(USER1.getId(), null);
        taskService.findOneByIndex(USER1.getId(), USER1_TASK_LIST.size() + 1);
    }

    @Test
    public void removeAll() {
        Assert.assertFalse(taskService.findAll().isEmpty());
        taskService.removeAll();
        Assert.assertTrue(taskService.findAll().isEmpty());
    }

    @Test
    public void removeAllByUserId() {
        Assert.assertFalse(taskService.findAll(USER1.getId()).isEmpty());
        taskService.removeAll(USER1.getId());
        Assert.assertTrue(taskService.findAll(USER1.getId()).isEmpty());
        thrown.expect(UserIdEmptyException.class);
        taskService.removeAll("");
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertEquals(USER1_TASK2, taskService.removeOneById(USER1.getId(), USER1_TASK2.getId()));
        Assert.assertFalse(taskService.findAll().contains(USER1_TASK2));
        Assert.assertNull(taskService.removeOneById(USER2.getId(), USER1_TASK1.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.removeOneById(null, USER1_TASK1.getId());

        thrown.expect(IdEmptyException.class);
        taskService.removeOneById(USER1.getId(), null);
    }

    @Test
    public void removeOneByIndexByUserId() {
        final int index = USER1_TASK_LIST.indexOf(USER1_TASK2);
        Assert.assertEquals(USER1_TASK2, taskService.removeOneByIndex(USER1.getId(), index));
        Assert.assertFalse(taskService.findAll().contains(USER1_TASK2));
        Assert.assertNull(taskService.removeOneByIndex(USER2.getId(), index));

        thrown.expect(UserIdEmptyException.class);
        taskService.removeOneByIndex(null, index);

        thrown.expect(IndexOutOfBoundsException.class);
        taskService.removeOneByIndex(USER1.getId(), null);
        taskService.removeOneByIndex(USER1.getId(), USER1_TASK_LIST.size() + 1);
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(taskService.existsById(USER1.getId(), USER1_TASK1.getId()));
        Assert.assertFalse(taskService.existsById(USER2.getId(), USER1_TASK1.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.existsById(null, USER1_TASK1.getId());

        thrown.expect(IdEmptyException.class);
        taskService.existsById(USER1.getId(), null);
    }

    @Test
    public void createTaskName() {
        @NotNull final Task task = taskService.create(USER1.getId(), "test task");
        Assert.assertEquals(task, taskService.findOneById(task.getId()));
        Assert.assertEquals("test task", task.getName());
        Assert.assertEquals(USER1.getId(), task.getUserId());

        thrown.expect(UserIdEmptyException.class);
        taskService.create(null, USER1_TASK1.getName());

        thrown.expect(NameEmptyException.class);
        taskService.create(USER1.getId(), null);
    }

    @Test
    public void createTaskNameDescription() {
        @NotNull final Task task = taskService.create(USER1.getId(), "test task", "test description");
        Assert.assertEquals(task, taskService.findOneById(task.getId()));
        Assert.assertEquals("test task", task.getName());
        Assert.assertEquals("test description", task.getDescription());
        Assert.assertEquals(USER1.getId(), task.getUserId());

        thrown.expect(UserIdEmptyException.class);
        taskService.create(null, USER1_TASK1.getName(), USER1_TASK1.getDescription());

        thrown.expect(NameEmptyException.class);
        taskService.create(USER1.getId(), null, USER1_TASK1.getDescription());

        thrown.expect(DescriptionEmptyException.class);
        taskService.create(USER1.getId(), "test task", null);
    }

    @Test
    public void updateById() {
        taskService.updateById(USER1.getId(), USER1_TASK2.getId(), "new name", "new description");
        Assert.assertEquals("new name", USER1_TASK2.getName());
        Assert.assertEquals("new description", USER1_TASK2.getDescription());

        thrown.expect(UserIdEmptyException.class);
        taskService.updateById(null, USER1_TASK2.getId(), "new name", "new description");

        thrown.expect(IdEmptyException.class);
        taskService.updateById(USER1.getId(), null, "new name", "new description");

        thrown.expect(NameEmptyException.class);
        taskService.updateById(USER1.getId(), USER1_TASK2.getId(), null, "new description");

        thrown.expect(ProjectNotFoundException.class);
        taskService.updateById(USER2.getId(), USER1_TASK2.getId(), "new name", "new description");
    }

    @Test
    public void updateByIndex() {
        final int index = TASK_LIST.indexOf(USER1_TASK2);
        taskService.updateByIndex(USER1.getId(), index, "new name", "new description");
        Assert.assertEquals("new name", USER1_TASK2.getName());
        Assert.assertEquals("new description", USER1_TASK2.getDescription());

        thrown.expect(UserIdEmptyException.class);
        taskService.updateByIndex(null, index, "new name", "new description");

        thrown.expect(IndexIncorrectException.class);
        taskService.updateById(USER1.getId(), null, "new name", "new description");

        thrown.expect(NameEmptyException.class);
        taskService.updateByIndex(USER1.getId(), index, null, "new description");

        thrown.expect(ProjectNotFoundException.class);
        taskService.updateByIndex(USER2.getId(), index, "new name", "new description");
    }

    @Test
    public void changeTaskStatusById() {
        Assert.assertEquals(Status.NOT_STARTED, USER1_TASK1.getStatus());
        taskService.changeTaskStatusById(USER1.getId(), USER1_TASK1.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, USER1_TASK1.getStatus());

        thrown.expect(UserIdEmptyException.class);
        taskService.changeTaskStatusById(null, USER1_TASK1.getId(), Status.IN_PROGRESS);

        thrown.expect(ProjectNotFoundException.class);
        taskService.changeTaskStatusById(USER2.getId(), USER1_TASK1.getId(), Status.IN_PROGRESS);
    }

    @Test
    public void changeTaskStatusByIndex() {
        final int index = TASK_LIST.indexOf(USER1_TASK2);
        Assert.assertEquals(Status.NOT_STARTED, USER1_TASK2.getStatus());
        taskService.changeTaskStatusByIndex(USER1.getId(), index, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, USER1_TASK2.getStatus());

        thrown.expect(UserIdEmptyException.class);
        taskService.changeTaskStatusByIndex(null, index, Status.IN_PROGRESS);

        thrown.expect(ProjectNotFoundException.class);
        taskService.changeTaskStatusByIndex(USER2.getId(), index, Status.IN_PROGRESS);
    }

    @Test
    public void findAllByProjectId() {
        Assert.assertEquals(USER1_TASK_LIST, taskService.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertNotEquals(USER2_TASK_LIST, taskService.findAllByProjectId(USER1.getId(), USER1_PROJECT1.getId()));

        thrown.expect(UserIdEmptyException.class);
        taskService.findAllByProjectId(null, USER2_PROJECT1.getId());
    }

}
