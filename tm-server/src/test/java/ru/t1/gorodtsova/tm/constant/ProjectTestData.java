package ru.t1.gorodtsova.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.model.Project;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static ru.t1.gorodtsova.tm.constant.UserTestData.*;

@UtilityClass
public final class ProjectTestData {

    @NotNull
    public final static Project USER1_PROJECT1 = new Project();

    @NotNull
    public final static Project USER1_PROJECT2 = new Project();

    @NotNull
    public final static Project USER1_PROJECT3 = new Project();

    @NotNull
    public final static Project USER2_PROJECT1 = new Project();

    @NotNull
    public final static Project ADMIN1_PROJECT1 = new Project();

    @NotNull
    public final static Project ADMIN1_PROJECT2 = new Project();

    @NotNull
    public final static List<Project> USER1_PROJECT_LIST = Arrays.asList(USER1_PROJECT1, USER1_PROJECT2, USER1_PROJECT3);

    @NotNull
    public final static List<Project> USER2_PROJECT_LIST = Collections.singletonList(USER2_PROJECT1);

    @NotNull
    public final static List<Project> ADMIN1_PROJECT_LIST = Arrays.asList(ADMIN1_PROJECT1, ADMIN1_PROJECT2);

    @NotNull
    public final static List<Project> PROJECT_LIST = new ArrayList<>();

    static {
        USER1_PROJECT_LIST.forEach(project -> project.setUserId(USER1.getId()));
        USER2_PROJECT_LIST.forEach(project -> project.setUserId(USER2.getId()));
        ADMIN1_PROJECT_LIST.forEach(project -> project.setUserId(ADMIN1.getId()));

        PROJECT_LIST.addAll(USER1_PROJECT_LIST);
        PROJECT_LIST.addAll(USER2_PROJECT_LIST);
        PROJECT_LIST.addAll(ADMIN1_PROJECT_LIST);

        for (int i = 0; i < PROJECT_LIST.size(); i++) {
            @NotNull final Project project = PROJECT_LIST.get(i);
            project.setId("t-0" + i);
            project.setName("project-" + i);
            project.setDescription("description of project-" + i);
        }
    }

}
