package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.gorodtsova.tm.api.repository.IProjectRepository;
import ru.t1.gorodtsova.tm.api.service.IProjectService;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.exception.entity.ProjectNotFoundException;
import ru.t1.gorodtsova.tm.exception.field.*;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.repository.ProjectRepository;

import static ru.t1.gorodtsova.tm.constant.ProjectTestData.*;
import static ru.t1.gorodtsova.tm.constant.UserTestData.USER1;
import static ru.t1.gorodtsova.tm.constant.UserTestData.USER2;

@Category(UnitCategory.class)
public final class ProjectServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        projectRepository.add(PROJECT_LIST);
    }

    @After
    public void tearDown() {
        projectRepository.removeAll();
    }

    @Test
    public void add() {
        projectService.removeAll();
        projectService.add(USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectService.findAll().get(0));
    }

    @Test
    public void addByUser() {
        projectService.removeAll();
        projectService.add(USER1.getId(), USER1_PROJECT1);
        Assert.assertEquals(USER1_PROJECT1, projectService.findAll().get(0));
        Assert.assertEquals(USER1.getId(), projectService.findAll().get(0).getUserId());
        Assert.assertNull(USER1.getId(), null);
        thrown.expect(UserIdEmptyException.class);
        projectService.add(null, USER1_PROJECT2);
    }

    @Test
    public void set() {
        Assert.assertFalse(projectService.findAll().isEmpty());
        projectService.set(USER1_PROJECT_LIST);
        Assert.assertEquals(USER1_PROJECT_LIST, projectService.findAll());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(PROJECT_LIST, projectService.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(USER1_PROJECT_LIST, projectService.findAll(USER1.getId()));
        Assert.assertNotEquals(USER1_PROJECT_LIST, projectService.findAll(USER2.getId()));
        thrown.expect(UserIdEmptyException.class);
        projectService.findAll("");
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertEquals(USER1_PROJECT1, projectService.findOneById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertNull(projectService.findOneById(USER1.getId(), USER2_PROJECT1.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.findOneById(null, USER1_PROJECT1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        projectService.findOneById(USER1.getId(), null);
    }

    @Test
    public void findOneByIndexByUserId() {
        final int index = USER1_PROJECT_LIST.indexOf(USER1_PROJECT2);
        Assert.assertEquals(USER1_PROJECT2, projectService.findOneByIndex(USER1.getId(), index));

        thrown.expect(UserIdEmptyException.class);
        projectService.findOneByIndex(null, index);

        thrown.expect(IndexIncorrectException.class);
        projectService.findOneByIndex(USER1.getId(), null);
        projectService.findOneByIndex(USER1.getId(), USER1_PROJECT_LIST.size() + 1);
    }

    @Test
    public void removeAll() {
        Assert.assertFalse(projectService.findAll().isEmpty());
        projectService.removeAll();
        Assert.assertTrue(projectService.findAll().isEmpty());
    }

    @Test
    public void removeAllByUserId() {
        Assert.assertFalse(projectService.findAll(USER1.getId()).isEmpty());
        projectService.removeAll(USER1.getId());
        Assert.assertTrue(projectService.findAll(USER1.getId()).isEmpty());
        thrown.expect(UserIdEmptyException.class);
        projectService.removeAll("");
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertEquals(USER1_PROJECT2, projectService.removeOneById(USER1.getId(), USER1_PROJECT2.getId()));
        Assert.assertFalse(projectService.findAll().contains(USER1_PROJECT2));
        Assert.assertNull(projectService.removeOneById(USER2.getId(), USER1_PROJECT1.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.removeOneById(null, USER1_PROJECT1.getId());

        thrown.expect(IdEmptyException.class);
        projectService.removeOneById(USER1.getId(), null);
    }

    @Test
    public void removeOneByIndexByUserId() {
        final int index = USER1_PROJECT_LIST.indexOf(USER1_PROJECT2);
        Assert.assertEquals(USER1_PROJECT2, projectService.removeOneByIndex(USER1.getId(), index));
        Assert.assertFalse(projectService.findAll().contains(USER1_PROJECT2));
        Assert.assertNull(projectService.removeOneByIndex(USER2.getId(), index));

        thrown.expect(UserIdEmptyException.class);
        projectService.removeOneByIndex(null, index);

        thrown.expect(IndexOutOfBoundsException.class);
        projectService.removeOneByIndex(USER1.getId(), null);
        projectService.removeOneByIndex(USER1.getId(), USER1_PROJECT_LIST.size() + 1);
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(projectService.existsById(USER1.getId(), USER1_PROJECT1.getId()));
        Assert.assertFalse(projectService.existsById(USER2.getId(), USER1_PROJECT1.getId()));

        thrown.expect(UserIdEmptyException.class);
        projectService.existsById(null, USER1_PROJECT1.getId());

        thrown.expect(IdEmptyException.class);
        projectService.existsById(USER1.getId(), null);
    }

    @Test
    public void createProjectName() {
        @NotNull final Project project = projectService.create(USER1.getId(), "test project");
        Assert.assertEquals(project, projectService.findOneById(project.getId()));
        Assert.assertEquals("test project", project.getName());
        Assert.assertEquals(USER1.getId(), project.getUserId());

        thrown.expect(UserIdEmptyException.class);
        projectService.create(null, USER1_PROJECT1.getName());

        thrown.expect(NameEmptyException.class);
        projectService.create(USER1.getId(), null);
    }

    @Test
    public void createProjectNameDescription() {
        @NotNull final Project project = projectService.create(USER1.getId(), "test project", "test description");
        Assert.assertEquals(project, projectService.findOneById(project.getId()));
        Assert.assertEquals("test project", project.getName());
        Assert.assertEquals("test description", project.getDescription());
        Assert.assertEquals(USER1.getId(), project.getUserId());

        thrown.expect(UserIdEmptyException.class);
        projectService.create(null, USER1_PROJECT1.getName(), USER1_PROJECT1.getDescription());

        thrown.expect(NameEmptyException.class);
        projectService.create(USER1.getId(), null, USER1_PROJECT1.getDescription());

        thrown.expect(DescriptionEmptyException.class);
        projectService.create(USER1.getId(), "test project", null);
    }

    @Test
    public void updateById() {
        projectService.updateById(USER1.getId(), USER1_PROJECT2.getId(), "new name", "new description");
        Assert.assertEquals("new name", USER1_PROJECT2.getName());
        Assert.assertEquals("new description", USER1_PROJECT2.getDescription());

        thrown.expect(UserIdEmptyException.class);
        projectService.updateById(null, USER1_PROJECT2.getId(), "new name", "new description");

        thrown.expect(IdEmptyException.class);
        projectService.updateById(USER1.getId(), null, "new name", "new description");

        thrown.expect(NameEmptyException.class);
        projectService.updateById(USER1.getId(), USER1_PROJECT2.getId(), null, "new description");

        thrown.expect(ProjectNotFoundException.class);
        projectService.updateById(USER2.getId(), USER1_PROJECT2.getId(), "new name", "new description");
    }

    @Test
    public void updateByIndex() {
        final int index = PROJECT_LIST.indexOf(USER1_PROJECT2);
        projectService.updateByIndex(USER1.getId(), index, "new name", "new description");
        Assert.assertEquals("new name", USER1_PROJECT2.getName());
        Assert.assertEquals("new description", USER1_PROJECT2.getDescription());

        thrown.expect(UserIdEmptyException.class);
        projectService.updateByIndex(null, index, "new name", "new description");

        thrown.expect(IndexIncorrectException.class);
        projectService.updateById(USER1.getId(), null, "new name", "new description");

        thrown.expect(NameEmptyException.class);
        projectService.updateByIndex(USER1.getId(), index, null, "new description");

        thrown.expect(ProjectNotFoundException.class);
        projectService.updateByIndex(USER2.getId(), index, "new name", "new description");
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertEquals(Status.NOT_STARTED, USER1_PROJECT1.getStatus());
        projectService.changeProjectStatusById(USER1.getId(), USER1_PROJECT1.getId(), Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, USER1_PROJECT1.getStatus());

        thrown.expect(UserIdEmptyException.class);
        projectService.changeProjectStatusById(null, USER1_PROJECT1.getId(), Status.IN_PROGRESS);

        thrown.expect(IdEmptyException.class);
        projectService.changeProjectStatusById(USER1.getId(), null, Status.IN_PROGRESS);

        thrown.expect(ProjectNotFoundException.class);
        projectService.changeProjectStatusById(USER2.getId(), USER1_PROJECT1.getId(), Status.IN_PROGRESS);
    }

    @Test
    public void changeProjectStatusByIndex() {
        final int index = PROJECT_LIST.indexOf(USER1_PROJECT2);
        Assert.assertEquals(Status.NOT_STARTED, USER1_PROJECT2.getStatus());
        projectService.changeProjectStatusByIndex(USER1.getId(), index, Status.IN_PROGRESS);
        Assert.assertEquals(Status.IN_PROGRESS, USER1_PROJECT2.getStatus());

        thrown.expect(UserIdEmptyException.class);
        projectService.changeProjectStatusByIndex(null, index, Status.IN_PROGRESS);

        thrown.expect(IndexIncorrectException.class);
        projectService.changeProjectStatusByIndex(USER1.getId(), null, Status.IN_PROGRESS);

        thrown.expect(ProjectNotFoundException.class);
        projectService.changeProjectStatusByIndex(USER2.getId(), index, Status.IN_PROGRESS);
    }

}
