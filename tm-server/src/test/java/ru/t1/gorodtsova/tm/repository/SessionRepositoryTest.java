package ru.t1.gorodtsova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.gorodtsova.tm.api.repository.ISessionRepository;
import ru.t1.gorodtsova.tm.marker.UnitCategory;

import static ru.t1.gorodtsova.tm.constant.SessionTestData.*;
import static ru.t1.gorodtsova.tm.constant.UserTestData.USER1;
import static ru.t1.gorodtsova.tm.constant.UserTestData.USER2;

@Category(UnitCategory.class)
public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Test
    public void add() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findAll().get(0));
    }

    @Test
    public void addByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1.getId(), USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findAll().get(0));
        Assert.assertEquals(USER1.getId(), sessionRepository.findAll().get(0).getUserId());
    }

    @Test
    public void addAll() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionRepository.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionRepository.findAll(USER1.getId()));
        Assert.assertNotEquals(USER1_SESSION_LIST, sessionRepository.findAll(USER2.getId()));
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        sessionRepository.add(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findOneById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertNotEquals(USER2_SESSION1, sessionRepository.findOneById(USER1.getId(), USER2_SESSION1.getId()));
        Assert.assertNull(sessionRepository.findOneById(USER1.getId(), USER2_SESSION1.getId()));
    }

    @Test
    public void findOneByIndexByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        sessionRepository.add(USER2_SESSION1);
        final int index1 = sessionRepository.findAll().indexOf(USER1_SESSION1);
        final int index2 = sessionRepository.findAll().indexOf(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.findOneByIndex(USER1.getId(), index1));
        Assert.assertNotEquals(USER2_SESSION1, sessionRepository.findOneByIndex(USER1.getId(), index2));
    }

    @Test
    public void removeByIdByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        sessionRepository.add(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.removeOneById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertFalse(sessionRepository.findAll().contains(USER1_SESSION1));
        Assert.assertTrue(sessionRepository.findAll().contains(USER2_SESSION1));
    }

    @Test
    public void removeByIndexByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        sessionRepository.add(USER2_SESSION1);
        final int index = sessionRepository.findAll().indexOf(USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.removeOneByIndex(USER1.getId(), index));
        Assert.assertFalse(sessionRepository.findAll().contains(USER1_SESSION1));
        Assert.assertTrue(sessionRepository.findAll().contains(USER2_SESSION1));
    }

    @Test
    public void removeByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        sessionRepository.add(USER2_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionRepository.removeOne(USER1.getId(), USER1_SESSION1));
        Assert.assertFalse(sessionRepository.findAll().contains(USER1_SESSION1));
        Assert.assertTrue(sessionRepository.findAll().contains(USER2_SESSION1));
    }

    @Test
    public void removeAllByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionRepository.findAll());
        sessionRepository.removeAll(USER2.getId());
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
        sessionRepository.removeAll(USER1.getId());
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER2_SESSION1);
        sessionRepository.removeAll(USER1.getId());
        Assert.assertFalse(sessionRepository.findAll().isEmpty());
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
        sessionRepository.add(USER1_SESSION1);
        Assert.assertTrue(sessionRepository.existsById(USER1_SESSION1.getId()));
        Assert.assertFalse(sessionRepository.existsById(USER2_SESSION1.getId()));
    }

}
