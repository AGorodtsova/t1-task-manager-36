package ru.t1.gorodtsova.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.junit.rules.ExpectedException;
import ru.t1.gorodtsova.tm.api.repository.ISessionRepository;
import ru.t1.gorodtsova.tm.api.service.ISessionService;
import ru.t1.gorodtsova.tm.exception.field.IdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.IndexIncorrectException;
import ru.t1.gorodtsova.tm.exception.field.ProjectIdEmptyException;
import ru.t1.gorodtsova.tm.exception.field.UserIdEmptyException;
import ru.t1.gorodtsova.tm.marker.UnitCategory;
import ru.t1.gorodtsova.tm.repository.SessionRepository;

import static ru.t1.gorodtsova.tm.constant.SessionTestData.*;
import static ru.t1.gorodtsova.tm.constant.UserTestData.USER1;
import static ru.t1.gorodtsova.tm.constant.UserTestData.USER2;

@Category(UnitCategory.class)
public final class SessionServiceTest {

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Rule
    @NotNull
    public final ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() {
        sessionRepository.add(SESSION_LIST);
    }

    @After
    public void tearDown() {
        sessionRepository.removeAll();
    }

    @Test
    public void add() {
        sessionService.removeAll();
        sessionService.add(USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionService.findAll().get(0));
    }

    @Test
    public void addByUser() {
        sessionService.removeAll();
        sessionService.add(USER1.getId(), USER1_SESSION1);
        Assert.assertEquals(USER1_SESSION1, sessionService.findAll().get(0));
        Assert.assertEquals(USER1.getId(), sessionService.findAll().get(0).getUserId());
        Assert.assertNull(USER1.getId(), null);
        thrown.expect(UserIdEmptyException.class);
        sessionService.add(null, USER1_SESSION2);
    }

    @Test
    public void set() {
        Assert.assertFalse(sessionService.findAll().isEmpty());
        sessionService.set(USER1_SESSION_LIST);
        Assert.assertEquals(USER1_SESSION_LIST, sessionService.findAll());
    }

    @Test
    public void findAll() {
        Assert.assertEquals(SESSION_LIST, sessionService.findAll());
    }

    @Test
    public void findAllByUserId() {
        Assert.assertEquals(USER1_SESSION_LIST, sessionService.findAll(USER1.getId()));
        Assert.assertNotEquals(USER1_SESSION_LIST, sessionService.findAll(USER2.getId()));
        thrown.expect(UserIdEmptyException.class);
        sessionService.findAll("");
    }

    @Test
    public void findOneByIdByUserId() {
        Assert.assertEquals(USER1_SESSION1, sessionService.findOneById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertNull(sessionService.findOneById(USER1.getId(), USER2_SESSION1.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.findOneById(null, USER1_SESSION1.getId());

        thrown.expect(ProjectIdEmptyException.class);
        sessionService.findOneById(USER1.getId(), null);
    }

    @Test
    public void findOneByIndexByUserId() {
        final int index = USER1_SESSION_LIST.indexOf(USER1_SESSION2);
        Assert.assertEquals(USER1_SESSION2, sessionService.findOneByIndex(USER1.getId(), index));

        thrown.expect(UserIdEmptyException.class);
        sessionService.findOneByIndex(null, index);

        thrown.expect(IndexIncorrectException.class);
        sessionService.findOneByIndex(USER1.getId(), null);
        sessionService.findOneByIndex(USER1.getId(), USER1_SESSION_LIST.size() + 1);
    }

    @Test
    public void removeAll() {
        Assert.assertFalse(sessionService.findAll().isEmpty());
        sessionService.removeAll();
        Assert.assertTrue(sessionService.findAll().isEmpty());
    }

    @Test
    public void removeAllByUserId() {
        Assert.assertFalse(sessionService.findAll(USER1.getId()).isEmpty());
        sessionService.removeAll(USER1.getId());
        Assert.assertTrue(sessionService.findAll(USER1.getId()).isEmpty());
        thrown.expect(UserIdEmptyException.class);
        sessionService.removeAll("");
    }

    @Test
    public void removeOneByIdByUserId() {
        Assert.assertEquals(USER1_SESSION2, sessionService.removeOneById(USER1.getId(), USER1_SESSION2.getId()));
        Assert.assertFalse(sessionService.findAll().contains(USER1_SESSION2));
        Assert.assertNull(sessionService.removeOneById(USER2.getId(), USER1_SESSION1.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.removeOneById(null, USER1_SESSION1.getId());

        thrown.expect(IdEmptyException.class);
        sessionService.removeOneById(USER1.getId(), null);
    }

    @Test
    public void removeOneByIndexByUserId() {
        final int index = USER1_SESSION_LIST.indexOf(USER1_SESSION2);
        Assert.assertEquals(USER1_SESSION2, sessionService.removeOneByIndex(USER1.getId(), index));
        Assert.assertFalse(sessionService.findAll().contains(USER1_SESSION2));
        Assert.assertNull(sessionService.removeOneByIndex(USER2.getId(), index));

        thrown.expect(UserIdEmptyException.class);
        sessionService.removeOneByIndex(null, index);

        thrown.expect(IndexOutOfBoundsException.class);
        sessionService.removeOneByIndex(USER1.getId(), null);
        sessionService.removeOneByIndex(USER1.getId(), USER1_SESSION_LIST.size() + 1);
    }

    @Test
    public void existsByIdByUserId() {
        Assert.assertTrue(sessionService.existsById(USER1.getId(), USER1_SESSION1.getId()));
        Assert.assertFalse(sessionService.existsById(USER2.getId(), USER1_SESSION1.getId()));

        thrown.expect(UserIdEmptyException.class);
        sessionService.existsById(null, USER1_SESSION1.getId());

        thrown.expect(IdEmptyException.class);
        sessionService.existsById(USER1.getId(), null);
    }

}
